package com.htphy.wx.net.netty.core.handle;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.DatagramPacket;

/**
 * UDP数据包数据输入处理器
 * <p>提取DatagramPacket中Bytebuf内容
 *
 * @author lw
 */
public class DatagramPacketHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        if (msg instanceof DatagramPacket) {
            //提取 Bytebuf 转发到下一处理器
            ctx.fireChannelRead(((DatagramPacket) msg).content());
        }
    }

}
